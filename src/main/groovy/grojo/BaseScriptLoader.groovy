package grojo

class BaseScriptLoader {

	private def groovyScriptEngine = new GroovyScriptEngine(".")

	def load(script, shell, grojoServlet){
		def codeString = ''
		def imports = ''

		def file = new File(script)
		if( file.isDirectory() ){
			file.eachFile{ f -> load(f.getPath(), shell, grojoServlet) }
			return
		}

		if( script.endsWith("groovy")){
			def newClass = groovyScriptEngine.loadScriptByName( file.getPath() )
			grojoServlet.modelInstantiator.register(newClass)
		}
		else if (script.endsWith("grojo")) {
			def code = new File(script).each {
				if( codeString == '' && it ==~ /import .+/){
					imports += it + "\n"
				}
				else{
					codeString += it + "\n"
				}
			}
			def closure = shell.evaluate("$imports{->${codeString}}")
			closure.delegate = this
			closure()				
		}
	}
}
