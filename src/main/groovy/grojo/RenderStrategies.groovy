package grojo

import grojo.json.JsonMarshaller
import groovy.xml.MarkupBuilder

class RenderStrategies {

	private RenderStrategies(){
	}
	
	private static INSTANCE = new RenderStrategies()
	static RenderStrategies getInstance(){
		return INSTANCE
	}
	
	def hasStrategy(name){
		return strategies.containsKey(name)
	}
	
	def strategies = [
		'text':{data, writer, context->
			writer.print(data)
		},
		'json':{data, writer, context->
			writer.print( new JsonMarshaller().marshall(data))
		},
		'view':{data, writer, context->
			context.boundVariables.html = new ViewBuilder(writer, context)
			def viewClosure = context.views[data].clone()
			viewClosure.delegate = context.boundVariables
			viewClosure()
		}
	]
	
}
