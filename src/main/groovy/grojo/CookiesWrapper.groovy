package grojo

class CookiesWrapper{
	def cookiesByName = [:]
	def response
	CookiesWrapper(request, response){
		this.response = response
		request.cookies.each{
			cookiesByName[it.name] = it
		}
	}
	
	Object get(String name){
		return cookiesByName[name]
	}
	
	void set(name, value){
		if( !cookiesByName.containsKey(name) ){
			cookiesByName[name] = value
		}
		response.addCookie(value)
	}
}