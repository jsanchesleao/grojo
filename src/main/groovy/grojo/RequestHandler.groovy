package grojo

class RequestHandler implements Comparable{
	
	private grojoServlet
	private urlMatcher
	private code
	private renderClosure
	private parameterClosure
	private boundVariables = [:]
	private modelInstantiator
	
	RequestHandler(GrojoServlet grojoServlet, String path, Closure code){
		this.grojoServlet = grojoServlet
		this.urlMatcher = new UrlMatcher(path)
		this.code = code
	}
	
	def matches(path){
		return urlMatcher.matches(path)
	}
	
	def process(req, res){
		res.setHeader("content-type", "text/html")
		def context = new RequestContext(
			views: grojoServlet.views, 
			request: req, 
			response: res, 
			boundVariables: urlMatcher.allBoundVariables(),
			beans: grojoServlet.beans,
			handler: this,
			MODEL: grojoServlet.modelInstantiator.safeCopy()
		)
		def requestCode = code.clone()
		requestCode.delegate = context
		requestCode()
	}

	public int compareTo(Object o) {
		if(! o instanceof RequestHandler ) return 0
		return urlMatcher.compareTo(o.urlMatcher);
	}

}
