package grojo

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class RequestContext {

	HttpServletRequest request
	HttpServletResponse response
	def boundVariables
	def views
	def renderer = RenderStrategies.getInstance()
	def beans
	def handler
	def dynamicMethods = [:]
	def fallbackMethod
	ModelInstantiator MODEL
	
	def render(params) throws GrojoException{
		params.each{ strategy, data->
			if(renderer.hasStrategy(strategy)){
				renderer.strategies[strategy](data, response.getWriter(), this)
			}
			else{
				throw new GrojoException("No such render strategy found: [$strategy]")
			}
		}
	}
	
	def useParameters(){
		request.getParameterMap().each{param, value ->
			boundVariables[param] = value[0]
		}
	}
	
	def useSession(){
		boundVariables['session'] = new SessionWrapper(session: request.session)
	}
	
	def useParameterArrays(){
		request.getParameterMap().each{param, value ->
			boundVariables[param] = value
		}
	}
	
	def useCookies(){
		boundVariables['cookies'] = new CookiesWrapper( request, response )
	}
	
	def useBean(name){
		boundVariables[name] = beans[name]
	}
	
	def parameter(name){
		return request.getParameter(name)
	}
	
	def rawData(){
		def val = new StringWriter()
		val << request.inputStream
		return val.toString()
	}
	
	
	def contentType(type){
		response.setContentType(type)
	}
	
	def forward(target){
		request.getRequestDispatcher(target).forward(request, response)
	}
	
	def redirect(target){
		response.sendRedirect(target)
	}
	
	def bindVar(name, value){
		boundVariables[name] = value
	}
	
	def getHelper(){
		return handler.grojoServlet.helperClosures
	}
	
	Object get(String name){
		if(boundVariables.containsKey(name)){
			return boundVariables[name]
		}
		else{
			return ''
		}
	}
	
	def addMethod(name, closure){
		closure.delegate = this
		dynamicMethods[name] = closure
	}
	
	def removeFallbackMethod(){
		fallbackMethod = null
	}
	
	Object invokeMethod(String name, Object args){
		if(dynamicMethods.containsKey(name)){
			return dynamicMethods[name].call(args)
		}
		else if(fallbackMethod != null){
			return fallbackMethod(args)
		}
		else throw new GrojoException("no helper function found [$name]")
	}
	
}
