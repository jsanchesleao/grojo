package grojo

class ModelInstantiator{

	def registry = [:]
	def packageName = ""
	def includes = [""]
	
	def register(classObject){
		registry[classObject.getName()] = classObject
	}

	Object invokeMethod(String name, Object args) {
		def className = packageName + name
		
		for( pack in includes){
			if( registry.containsKey( pack + className) ){
				return registry[pack + className].newInstance(args)
			}	
		}

		throw new RuntimeException("Could not instantiate class [${className}]")
	}

	def getProperty(String name){
		return new ModelInstantiator( includes: includes, registry: registry, packageName: (packageName + name + "."))
	}

	def include(apackage){
		includes.push(apackage + ".")
	}

	def safeCopy(){
		def newIncludes = []
		newIncludes.addAll(this.includes)
		return new ModelInstantiator( includes: newIncludes, registry: registry, packageName: packageName )
	}

}