package grojo

import javax.servlet.Servlet
import javax.servlet.http.*

import org.mortbay.jetty.Server
import org.mortbay.jetty.servlet.Context
import org.mortbay.jetty.servlet.ServletHolder
import org.mortbay.jetty.webapp.WebAppContext
import org.mortbay.resource.ResourceCollection

class Start{

	private Server server
	private pluginDirectories = []
	static final def DEFAULT_PORT_PROPERTY = "grojo.http.port"
	private static final def DEFAULT_PORT = 9005 
	static final def CORE_PLUGINS_DIR_PROPERTY = "grojo.core.plugins.dir"
	private static final def DEFAULT_CORE_PLUGINS_DIR = 'plugins'
	def scriptLoader
	def grojoServlet
	
	static Start INSTANCE = null
	
	void addPluginDirectory(dirname){
		pluginDirectories.add(new File(dirname))
	}
	
	def start(String[] args){
		def beforeStart = System.nanoTime()
		
		pluginDirectories.add( corePlugins() )
		server = new Server(systemPort())
		def root = new Context(server,"/",Context.SESSIONS);

		grojoServlet = new GrojoServlet()
		scriptLoader = new ScriptLoader(grojoServlet: grojoServlet, pluginDirectories: pluginDirectories)
		args.each{
			scriptLoader.load(it)
		}
		grojoServlet.normalizeHandlers()
		
		root.addServlet(new ServletHolder(grojoServlet), "/*")
		server.start()

		def afterStart = System.nanoTime()
		println "Server startup in ${(afterStart-beforeStart)/1000000}ms"
	}
	
	private corePlugins(){
		def systemProperty = System.getProperty(CORE_PLUGINS_DIR_PROPERTY) 
		if( systemProperty != null ){
			return systemProperty
		}
		return 'plugins'
	}
	
	private systemPort(){
		def port = System.getProperty(DEFAULT_PORT_PROPERTY) as Integer
		if(port){
			return port
		}
		return DEFAULT_PORT
	}
	
	void stop(){
		this.server.stop()
	}
	
	static Start getInstance(){
		if(INSTANCE == null){
			INSTANCE = new Start()
		}
		return INSTANCE
	}
	
	static void main(String[] args){
		Start.getInstance().start(args)
	}

}
