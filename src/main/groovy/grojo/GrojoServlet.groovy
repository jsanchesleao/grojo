package grojo

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


class GrojoServlet extends HttpServlet{

	def getRequestHandlers = []
	def postRequestHandlers = []
	def publicResourcesDir = 'public'
	def views = [:]
	def beans = [:]
	def helperClosures = [:]
	def modelInstantiator = new ModelInstantiator()
	
	def normalizeHandlers(){
		getRequestHandlers.sort()
		postRequestHandlers.sort()
	}
	
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res){
		def resource = new File(publicResourcesDir + req.getRequestURI())
		if(resource.isFile()){
			def input = new FileInputStream(resource)
			res.outputStream << input
			input.close()
		}
		else{
			process(getRequestHandlers, req, res)
		}
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res){
		process(postRequestHandlers, req, res)
	}
	
	private process(handlers, req, res){
		for(handler in handlers){
			if(handler.matches(req.getRequestURI())){
				handler.process(req, res)
				return
			}
		}
	}

}
