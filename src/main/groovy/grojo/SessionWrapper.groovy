package grojo

class SessionWrapper{
	def session
	Object get(String name){
		return session.getAttribute(name)
	}
	
	void set(name, value){
		session.setAttribute(name, value)
	}
}
