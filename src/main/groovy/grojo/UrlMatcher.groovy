package grojo

class UrlMatcher implements Comparable{

	private Map boundVariables = [:]
	private List patternContext
	private leftWideWildcard
	private rightWideWildcard
	
	UrlMatcher(String pattern){
		this.patternContext = removeTrailingEmptyStrings(pattern.split('/') as List)
		if(patternContext.size()!=0){
			if(patternContext[0] == "**"){
				leftWideWildcard = true
				patternContext.remove(0)
			}
			else if(patternContext.size() != 0 && patternContext[-1] == "**"){
				rightWideWildcard = true
				patternContext.remove(patternContext.size()-1)
			}
		}
	}
	
	boolean matches(String url){
		def match = false
		def urlPaths = removeTrailingEmptyStrings( url.split('/') as List)
		if(leftWideWildcard){
			match = parseLeftWideWildcard(urlPaths)
		}
		else if(rightWideWildcard){
			match = parseRightWideWildcard(urlPaths)
		}
		else{
			match = parseFixedSizeMatching(urlPaths)
		}
		return match
	}
	
	private boolean parseRightWideWildcard(urlPaths){
		if(urlPaths.size() < patternContext.size()){
			return false
		}
		def obj = rightWideWildcardUrlPath(urlPaths)
		return parseFixedSizeMatching(obj)
	}
	
	private def rightWideWildcardUrlPath(urlPaths){
		return urlPaths[0..(patternContext.size() - 1)]
	}

	private boolean parseLeftWideWildcard(urlPaths) {
		if(urlPaths.size() < patternContext.size()){
			return false
		}
		return parseFixedSizeMatching(leftWideWildcardUrlPath(urlPaths))
	}
	
	private def leftWideWildcardUrlPath(urlPaths){
		if(patternContext.size() == 0) return []
		return urlPaths[(urlPaths.size()-patternContext.size())..-1]
	}

	private boolean parseFixedSizeMatching(urlPaths) {
		def match = true
		if(urlPaths.size() != patternContext.size()) return false
		[urlPaths, patternContext].transpose().each{ tested, real ->
			if( real.startsWith(':') ){
				boundVariables[real[1..-1]] = tested
			}
			else if( !(tested ==~ real.replaceAll(/\*/, '.*?')) ){
				match = false
			}
		}
		return match
	}
	
	def bound(name){
		return boundVariables[name]
	}
	
	def allBoundVariables(){
		return boundVariables
	}
	
	def isMoreSpecificThan(otherMatcher){
		return compareTo(otherMatcher) < 0
	}
	
	int priority(){
		def weight = 0
		def pow = 0
		patternContext.each{
			weight -= 2 ** pow
			pow++
			if(it =~ /\*/){
				weight += 2 ** pow
			}
			pow++
			if( it ==~ /:\w+/){
				weight += 2 ** pow
			}
			pow++
		}
		if(leftWideWildcard || rightWideWildcard){
			weight += 2 ** pow
		}
		return weight
	}
	
	private removeTrailingEmptyStrings(List list){
		if(list[0] == ""){
			list.remove(0)
			return removeTrailingEmptyStrings(list)
		}
		return list
	}
	
	private isWideWildcard(){
		return leftWideWildcard || rightWideWildcard
	}

	@Override
	public int hashCode() {
		return this.patternContext.hashCode()
	}

	public int compareTo(Object o) {
		if(! o instanceof UrlMatcher ) return -1
		if(isWideWildcard() && !o.isWideWildcard()){
			return 1
		}
		else if( !isWideWildcard() && o.isWideWildcard()){
			return -1
		}
		def priorityDelta = priority() - o.priority()
		if( priorityDelta != 0 ) return priorityDelta
		return patternContext.join("").compareTo(o.patternContext.join(""))
	}

	@Override
	public String toString() {
		return "${leftWideWildcard ?'/**' : ''}/${patternContext.join('/')}${rightWideWildcard ? '/**' : ''}"
	}
	
	
}
