package grojo.plugin

import grojo.BaseScriptLoader;
import grojo.RenderStrategies;
import grojo.RequestContext

class PluginsLoader extends BaseScriptLoader{
	
	def grojoServlet
	def pluginDirectories
	
	def renderStrategy(name, closure){
		RenderStrategies.getInstance().strategies[name] = closure
	}
	
	def bean(name, closure){
		grojoServlet.beans[name] = closure()
	}
	
	def loadJar(jarName){
		def classLoader = PluginsLoader.class.classLoader.systemClassLoader
		pluginDirectories.each {
			def jar = new File(it, jarName)
			if(jar.exists()){
				classLoader.addURL( jar.toURI().toURL() )
			}
		}
	}
	
	def newInstance(String className, Object... parameters){
		return Class.forName(className).newInstance(parameters)
	}

}