package grojo

import grojo.plugin.PluginsLoader

class ScriptLoader extends BaseScriptLoader{
	
	def grojoServlet
	def pluginsLoader = null
	def pluginDirectories
	private shell = new GroovyShell()
	
	def getBeans(){
		return grojoServlet.beans
	}
	
	def bootstrap(Closure closure){
		closure.delegate = this
		closure()
	}
	
	def apply_plugin(name){
		def found = false;
		if(pluginsLoader == null){
			pluginsLoader = new PluginsLoader(grojoServlet: grojoServlet, pluginDirectories: pluginDirectories)
		}
		pluginDirectories.each{
			File plugin = new File(it, "${name}.grojo")
			if( plugin.exists() && plugin.isFile() ){
				pluginsLoader.load( plugin.absolutePath, shell, grojoServlet )
				found = true
			}
		}
		if(!found){
			throw new GrojoException("could not load plugin [$name]")
		}
	}
	
	def get(path, handler){
		def requestHandler = new RequestHandler(grojoServlet, path, handler)
		grojoServlet.getRequestHandlers.add(requestHandler)
	}
	
	def post(path, handler){
		def requestHandler = new RequestHandler(grojoServlet, path, handler)
		grojoServlet.postRequestHandlers.add(requestHandler)
	}
	
	def any(path, handler){
		get(path, handler)
		post(path, handler)
	}
	
	def view(name, value){
		grojoServlet.views[name] = value
	}
	
	def resources(dirName){
		grojoServlet.publicResourcesDir = dirName
	}
	
	def load(script){
		load(script, shell, grojoServlet)
	}
	
	def helper(name, code){
		grojoServlet.helperClosures[name] = code
	}

}
