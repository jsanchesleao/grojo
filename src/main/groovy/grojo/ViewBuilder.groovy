package grojo

import java.io.Writer;
import java.util.Map;

import groovy.xml.MarkupBuilder

class ViewBuilder extends MarkupBuilder{
	
	def requestContext


	public ViewBuilder(Writer writer, RequestContext requestContext) {
		super(writer)
		this.requestContext = requestContext
	}

	@Override
	protected Object createNode(Object name) {
		return super.createNode(name);
	}	

	@Override
	protected Object createNode(Object name, Object value) {
		if( name == 'view' ){
			def view = requestContext.views[value].clone()
			view.delegate = requestContext
			
			requestContext.fallbackMethod = { tagname, args ->
				tagname(args)
			};
		
			def result = view()
			requestContext.removeFallbackMethod()
			return result
		}
		return super.createNode(name, value);
	}
	
}
