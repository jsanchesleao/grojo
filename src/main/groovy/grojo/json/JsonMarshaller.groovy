package grojo.json

class JsonMarshaller {

	String marshall(Map values){
		def entries = values.collect { key, value-> /"$key":"$value"/ }
		return "{${entries.join(',')}}"
	}
		
}
