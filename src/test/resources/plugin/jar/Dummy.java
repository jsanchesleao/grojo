package dummy;

public class Dummy{

	public String getDummyMessage(){
		return "DUMMY";
	}

	public static void main(String... args){
		System.out.println( new Dummy().getDummyMessage() );
	}
}
