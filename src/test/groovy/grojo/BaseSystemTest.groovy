package grojo

import static org.junit.Assert.*

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver

class BaseSystemTest {

	WebDriver driver = new HtmlUnitDriver()
	def grojo = Start.getInstance()
	
	def checkOccurrency(string){
		def source = driver.getPageSource()
		if(! (source =~ string) ){
			fail("wrong content:[$source]")
		}
	}

	def countOccurrencies(string, expectedCount){
		def source = driver.getPageSource()
		def matches = (source =~ string)
		assertEquals(expectedCount, matches.size())
	}

	def httpRequest(urlString, String data){
		// Send data
		URL url = new URL(urlString);
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write(data);
		wr.flush();

		// Get the response
		def response = new StringWriter()
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		response << rd
		wr.close();
		rd.close();
		return response.toString()
	}

}
