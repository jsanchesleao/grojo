package grojo

import org.junit.Test
import static org.junit.Assert.*

class GroovyCaseStudyTest {

	@Test
	void test_regex_all_occurencies(){
		def text = 'abc123xyz'
		def result = (text =~ /\d/)
		
		assertEquals( '1', result[0])
		assertEquals( '2', result[1])
		assertEquals( '3', result[2])
		assertEquals( 3, result.size() )
	}
	
	@Test
	void test_regex_extract(){
		def text = 'xxxa1a2a3xxx'
		def result = (text =~ /a(\d)/)
		
		assertEquals( '1', result[0][1])
		assertEquals( '2', result[1][1])
		assertEquals( '3', result[2][1])
		assertEquals( 3, result.size() )
	}
	
	@Test
	void test_set_ordering(){
		Set set = []
		set.add('ax')
		set.add('b')
		set.add('ax')
		
		assertEquals(2, set.size())
		assertTrue( set instanceof HashSet )
		
		def list = set.sort{ it.size() }
		assertEquals( ['b', 'ax'], list)
	}
	
	@Test
	void test_join(){
		List list = ['foo', 'bar', 'zoo']
		assertEquals('foo, bar, zoo', list.join(", "))
	}
	
	@Test
	void test_system_property(){
		def prop = System.getProperty("invalid.property")
		assertEquals(null, prop)
	}
	
}
