package grojo

import org.junit.Test
import org.junit.Before 
import static org.junit.Assert.*

class ModelInstantiatorTest{

	def instantiator

	@Before
	void setup(){
		instantiator = new ModelInstantiator()		
	}
	
	@Test
	void test_instantiating_object(){
		instantiator.register( java.lang.String.class )
		def obj = instantiator.java.lang.String("Teste")
		assertEquals( obj, "Teste")
	}

	@Test
	void test_importing_package(){
		instantiator.register( java.lang.String.class )
		instantiator.include("java.lang")

		def obj = instantiator.String("Imported")
		assertEquals( "Imported", obj )
	}
	
}