package grojo.json

import org.junit.Test
import static org.junit.Assert.*

class JsonMarshallerTest {

	@Test
	void test_create_with_single_entry(){
		def marshaller = new JsonMarshaller()
		assertEquals( /{"foo":"1"}/, marshaller.marshall(['foo':1]) )
	}
	
	@Test
	void test_create_with_multiple_entries(){
		def marshaller = new JsonMarshaller()
		def json = marshaller.marshall(['foo':1, 'bar':2])
		
		assertTrue( (json =~ /"foo":"1"/).size() != 0 )
		assertTrue( (json =~ /"bar":"2"/).size() != 0 )
		assertTrue( json ==~ /\{.*?\}/ )
	}
	
}
