package grojo;

import static org.junit.Assert.*;

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import static org.junit.Assert.*

class GrojoSystemTest extends BaseSystemTest{
	
	@Before
	void setup(){
		System.setProperty("bootstrap", "none")
		System.setProperty("grojo.http.port", "9001")
		grojo.start('src/test/resources/test.grojo')
		driver.manage().deleteAllCookies()
	}
	
	@Test
	void test_bootstrap(){
		assertEquals("done", System.getProperty("bootstrap"))
	}
	
	@Test
	void test_get_simple_page(){
		driver.get('http://localhost:9001/test')
		checkOccurrency(/It works GET/)
	}
	
	@Test
	void test_get_parameterized_page(){
		driver.get('http://localhost:9001/param/42')
		checkOccurrency(/this is page 42/)
	}
	
	@Test
	void test_post_simple_page(){
		driver.get('http://localhost:9001/test')
		driver.findElement(By.name("testPost")).submit()
		
		checkOccurrency(/It works POST/)
	}
	
	@Test
	void test_any_http_method(){
		driver.get('http://localhost:9001/any')
		checkOccurrency(/It works ANY/)
		
		driver.findElement(By.name("submit")).submit()
		checkOccurrency(/It works ANY/)
	}
	
	@Test
	void test_url_precedencies(){
		driver.get('http://localhost:9001/matches')
		checkOccurrency(/strict match/)
		countOccurrencies('RESULT', 1)

		driver.get('http://localhost:9001/matchxxx')
		checkOccurrency(/wildcard match/)
		countOccurrencies('RESULT', 1)
		
		driver.get('http://localhost:9001/match/submatch')
		checkOccurrency(/strict submatch/)
		countOccurrencies('RESULT', 1)
		
		driver.get('http://localhost:9001/match/anything')
		checkOccurrency(/parameterized submatch/)
		countOccurrencies('RESULT', 1)
		
		driver.get('http://localhost:9001/param/match')
		checkOccurrency(/deep parameter/)
		countOccurrencies('RESULT', 1)
		
		driver.get('http://localhost:9001/wildxx/match')
		checkOccurrency(/deep wildcard/)
		countOccurrencies('RESULT', 1)
	}
	
	@Test
	void test_check_url_get_parameters(){
		driver.get('http://localhost:9001/test?msg=foo')
		checkOccurrency(/urlparameter msg = foo/)
	}
	
	@Test
	void test_json_support(){
		driver.get('http://localhost:9001/json')
		checkOccurrency(/"foo":"1"/)
		checkOccurrency(/"bar":"2"/)
	}
	
	@Test
	void test_public_resources(){
		driver.get('http://localhost:9001/resource.txt')
		checkOccurrency(/this is a resource/)
	}
	
	@Test
	void test_use_of_parameters_as_variables(){
		driver.get('http://localhost:9001/parameters?name=foo')
		checkOccurrency(/param foo/)
	}

	@Test
	void test_use_of_parameters_as_arrays(){
		driver.get('http://localhost:9001/arrayParameters?name=foo&name=bar')
		checkOccurrency(/param foo bar/)
	}
		
	@Test
	void test_views(){
		driver.get('http://localhost:9001/toView')
		checkOccurrency(/I am renderized from within a view object/)
		
		driver.get('http://localhost:9001/toView?name=foo')
		checkOccurrency(/the name is foo/)
	}
	
	@Test
	void test_session(){
		driver.get('http://localhost:9001/session/put')
		countOccurrencies('foo', 0)
		driver.get('http://localhost:9001/session/use')
		countOccurrencies('foo', 1)
	}
	
	@Test
	void test_post_data(){
		assertEquals('foo', httpRequest("http://localhost:9001/rawdata", "foo"))
	}
	
	@Test(expected=GrojoException.class)
	void test_unregistered_render_strategy_use(){
		def context = new RequestContext()
		context.render( foo: 'bar' )
	}
	
	@Test
	void test_include_script(){
		driver.get('http://localhost:9001/include')
		countOccurrencies('foo', 1)
	}
	
	@Test
	void test_use_cookies(){
		driver.get('http://localhost:9001/cookies/put')
		def afterCookie = driver.manage().getCookieNamed("foo")
		assertNotNull(afterCookie)
		assertEquals('bar', afterCookie.value)
	}
	
	@Test
	void test_use_beans(){
		driver.get('http://localhost:9001/beans')
		checkOccurrency(/useBean bar ; directVariable bar/);
	}
	
	@Test
	void test_forward(){
		driver.get('http://localhost:9001/forward')
		checkOccurrency(/target/);
	}
	
	@Test
	void test_redirect(){
		driver.get('http://localhost:9001/redirect')
		checkOccurrency(/target/);
		assertEquals("http://localhost:9001/test/target", driver.currentUrl)
	}
	
	@Test
	void test_helper(){
		driver.get('http://localhost:9001/useHelper/10/20')
		checkOccurrency(/30/);
	}
	
	@Test
	void test_nested_views(){
		driver.get('http://localhost:9001/nestedViews')
		checkOccurrency(/inner component with 42/);
	}

	@Test
	void test_load_directory(){
		driver.get('http://localhost:9001/directoryImport')
		checkOccurrency(/works/)
	}

	@Test
	void test_load_model_class(){
		driver.get('http://localhost:9001/modelclass')
		checkOccurrency(/VALUE/)
	}
	
	@After
	void teardown(){
		driver.close()
		grojo.stop()
	}
	
}
