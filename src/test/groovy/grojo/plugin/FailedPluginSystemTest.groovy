package grojo.plugin

import grojo.GrojoException
import grojo.Start

import org.junit.Test
import static org.junit.Assert.*

class FailedPluginSystemTest {

	@Test
	void test_initialize_failing_loading_inexistent_plugin(){
		def grojo = new Start()
		grojo.addPluginDirectory('src/test/resources')
		System.setProperty("grojo.http.port", "9005")
		try{
			grojo.start('src/test/resources/failplugin.grojo')
			fail('should throw exception')
		}
		catch(GrojoException e){
			assertEquals("could not load plugin [inexistent]", e.getMessage())
		}
	}
	
}
