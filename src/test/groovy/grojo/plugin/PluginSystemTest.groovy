package grojo.plugin

import grojo.Start

import static org.junit.Assert.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver

class PluginSystemTest {
	
	WebDriver driver = new HtmlUnitDriver()
	Start grojo = new Start()
	
	@Before
	void setup(){
		grojo.addPluginDirectory('src/test/resources')
		System.setProperty("grojo.http.port", "9005")
		grojo.start('src/test/resources/plugin.grojo')
	}
	
	@Test
	void successful_load(){
		driver.get("http://localhost:9005/upper")
		countOccurrencies("HELLO", 1)
	}
	
	@Test
	void test_bean_plugin(){
		driver.get("http://localhost:9005/bean")
		countOccurrencies(42, 1)
	}
	
	@Test
	void test_jar_plugin(){
		driver.get("http://localhost:9005/jar")
		countOccurrencies("DUMMY", 1)
	}
	
	@After
	void teardown(){
		grojo.stop()
	}
	
	private countOccurrencies(string, expectedCount){
		def source = driver.getPageSource()
		def matches = (source =~ string)
		assertEquals("page source:\n$source", expectedCount, matches.size())
	}

}
