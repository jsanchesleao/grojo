package grojo

import org.junit.Test
import static org.junit.Assert.*

class UrlMatcherTest {
	
	@Test
	void test_create_empty_matcher(){
		def matcher = new UrlMatcher('')
		assertTrue(matcher.matches(''))
	}

	@Test
	void test_simple_match(){
		def matcher = new UrlMatcher('/test')
		assertTrue( matcher.matches('/test') )
		assertFalse( matcher.matches('/testing') )
	}
	
	@Test
	void test_simple_unmatch(){
		def matcher = new UrlMatcher('/test')
		assertFalse( matcher.matches('/wrong') )
	}
	
	@Test
	void test_wildcard_match(){
		def matcher = new UrlMatcher('/test*')
		assertTrue( matcher.matches('/testxxx') )
	}
	
	@Test
	void test_wide_wildcard_match_at_beginning(){
		def matcher = new UrlMatcher('**/bar')
		assertTrue( matcher.matches('/test/bar') )
		assertTrue( matcher.matches('/test/foo/bar') )
		assertFalse( matcher.matches('/test/foo/bar/zoo') )
		assertFalse( matcher.matches('/') )
	}
	
	@Test
	void test_wide_wildcard_match_at_end(){
		def matcher = new UrlMatcher('/foo/**')
		assertTrue( matcher.matches('/foo/bar') )
		assertTrue( matcher.matches('/foo/bar/zoo') )
		assertFalse( matcher.matches('/zoo/bar/zoo') )
		assertFalse( matcher.matches('/') )
	}
	
	@Test
	void test_complex_match(){
		def matcher = new UrlMatcher('/foo*/:bar/zoo')
		assertTrue( matcher.matches('/foo/woo/zoo') )
		assertEquals( 'woo', matcher.bound('bar') )
		
		assertTrue( matcher.matches('/foozz/wee/zoo') )
		assertEquals( 'wee', matcher.bound('bar') )
		
		assertFalse( matcher.matches('/woo/wee/zoo') )
		assertFalse( matcher.matches('/foo/wee/zooxx') )
	}
	
	@Test
	void test_all_bound_variables(){
		def matcher = new UrlMatcher('/foo/:bar/:zoo')
		assertTrue( matcher.matches('/foo/one/two') )
	
		def bounds = matcher.allBoundVariables()
		assertEquals(2, bounds.size() )
		assertEquals('one', bounds['bar'])
		assertEquals('two', bounds['zoo'])
		
	}
	
	@Test
	void test_same_deep_pattern_precedency(){
		def simple = new UrlMatcher('/one/two')
		def wildcard = new UrlMatcher('/one/two*')
		def parameterized = new UrlMatcher('/one/:two')
		
		assertTrue( simple.isMoreSpecificThan(wildcard) )
		assertTrue( simple.isMoreSpecificThan(parameterized) )
		assertTrue( wildcard.isMoreSpecificThan(parameterized) )
		
		assertFalse( wildcard.isMoreSpecificThan(simple) )
		assertFalse( parameterized.isMoreSpecificThan(simple) )
		assertFalse( parameterized.isMoreSpecificThan(wildcard) )
	}
	
	@Test
	void test_different_deep_pattern_precedency(){
		def oneDeep = new UrlMatcher('/one')
		def twoDeep = new UrlMatcher('/one/two')
		
		assertTrue( twoDeep.isMoreSpecificThan(oneDeep) )
		assertFalse( oneDeep.isMoreSpecificThan(twoDeep) )
	}
	
	@Test
	void test_wide_wildcard_pattern_precedency_over_others(){
		def wideWildcard = new UrlMatcher("**/foo/bar")
		def normal = new UrlMatcher("/foo")
		def wildcard = new UrlMatcher("/foo*/bar")
		def parameterized = new UrlMatcher("/foo/:bar")
		
		assertTrue(normal.isMoreSpecificThan(wideWildcard))
		assertTrue(wildcard.isMoreSpecificThan(wideWildcard))
		assertTrue(parameterized.isMoreSpecificThan(wideWildcard))
		
		assertFalse(wideWildcard.isMoreSpecificThan(normal))
		assertFalse(wideWildcard.isMoreSpecificThan(wildcard))
		assertFalse(wideWildcard.isMoreSpecificThan(parameterized))
	}
	
	@Test
	void test_wide_wildcard_pattern_precedency_over_other_wide_wildcards(){
		def simple = new UrlMatcher("**/foo/bar")
		def withWildcards = new UrlMatcher("/foo*/bar/**")
		def withParameters = new UrlMatcher("/foo/:bar/**")
		
		assertTrue(simple.isMoreSpecificThan(withWildcards))
		assertTrue(simple.isMoreSpecificThan(withParameters))
		assertTrue(withWildcards.isMoreSpecificThan(withParameters))
		
		assertFalse(withWildcards.isMoreSpecificThan(simple))
		assertFalse(withParameters.isMoreSpecificThan(simple))
		assertFalse(withParameters.isMoreSpecificThan(withWildcards))
	}
	
	@Test
	void test_wide_wildcard_pattern_precedency_over_variable_bind(){
		def wideWildcard = new UrlMatcher("/**")
		def parameterized = new UrlMatcher("/foo/:bar")
		
		assertTrue(parameterized.isMoreSpecificThan(wideWildcard))
		
		assertFalse(wideWildcard.isMoreSpecificThan(parameterized))
	}
	
	@Test
	void test_wide_wildcard_variable_binding(){
		def matcher = new UrlMatcher("**/:foo/bar*")
		assertTrue(matcher.matches("/one/two/three/barz"))
		
		def bound = matcher.allBoundVariables()
		assertEquals('three', bound['foo'])
	}
	
	@Test
	void test_wild_wildcard_matches_anything(){
		def leftMatcher = new UrlMatcher("/**")
		assertTrue(leftMatcher.matches("/foo/bar"))
		
		def rightMatcher = new UrlMatcher("**/")
		assertTrue(rightMatcher.matches("/foo/bar"))
	}
	
	@Test
	void test_equality(){
		def first = new UrlMatcher('/foo')
		def second = new UrlMatcher('/foo')
		def third = new UrlMatcher('/bar')
		
		assertTrue( first == second )
		assertTrue( first.hashCode() == second.hashCode() )
		
		assertFalse( first == third )
		assertFalse( first == null )
		assertFalse( first == "" )
	}
	
	@Test
	void test_to_string(){
		assertEquals('/foo', new UrlMatcher('/foo').toString())
		assertEquals('/foo/bar', new UrlMatcher('/foo/bar').toString())
		assertEquals('/foo/:bar', new UrlMatcher('/foo/:bar').toString())
		assertEquals('/foo*/bar', new UrlMatcher('/foo*/bar').toString())
		assertEquals('/**/foo/bar', new UrlMatcher('/**/foo/bar').toString())
		assertEquals('/foo/bar/**', new UrlMatcher('/foo/bar/**').toString())
	}
}